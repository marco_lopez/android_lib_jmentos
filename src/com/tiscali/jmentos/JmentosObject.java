package com.tiscali.jmentos;

import java.lang.annotation.*;

import com.tiscali.jmentos.convertors.*;

@Target(value={ElementType.FIELD, ElementType.METHOD})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface JmentosObject {
	public String name() default "";
	public Class<? extends EntityConvertor> convertor() default Jmentos.class;
}
