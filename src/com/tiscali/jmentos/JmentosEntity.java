package com.tiscali.jmentos;

import java.lang.annotation.*;

import com.tiscali.jmentos.convertors.EntityConvertor;

@Target(value = { ElementType.TYPE })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JmentosEntity {
	public String name() default "";

	public enum Type {
		/**
		 * The entity represents a object
		 */
		JmentosObject, /**
		 * The entity represents a collection
		 */
		JmentosArray;
	}

	public Type type() default Type.JmentosObject;

	public Class<? extends EntityConvertor> convertor() default Jmentos.class;
	
	public String[] required() default {};
}
