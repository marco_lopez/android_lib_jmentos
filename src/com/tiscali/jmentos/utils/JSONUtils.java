package com.tiscali.jmentos.utils;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.tiscali.jmentos.Commons;

public class JSONUtils {
	private static final String tag = JSONUtils.class.getCanonicalName();

	public static final boolean isJSONArray(String name, JSONObject jsonParent) {
		JSONArray child = jsonParent.optJSONArray(name);
		if (null != child)
			return true;
		return false;
	}

	public static final boolean isJSONObject(String name, JSONObject jsonParent) {
		JSONObject child = jsonParent.optJSONObject(name);
		if (null != child)
			return true;
		return false;
	}

	public static String[] toStringArray(JSONArray jsonArray) {
		int length = jsonArray.length();
		String[] array = new String[length];
		for (int i = 0; i < length; i++) {
			try {
				String name = jsonArray.getString(i);
				array[i] = name;
			} catch (JSONException e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
			}
		}
		return array;
	}

	public static String[] toStringArray(Collection<?> collection) {
		Object[] objects = collection.toArray();
		String[] strings = new String[objects.length];
		for (int i = 0; i < strings.length; i++) {
			strings[i] = objects[i].toString();
		}
		return strings;
	}
}
