package com.tiscali.jmentos.utils;

import org.json.JSONObject;

import com.tiscali.jmentos.JmentosEntity;

public class ClassUtils {

	/**
	 * Return true if class is primitive, primitive wrapper or assignable from
	 * {@link String}
	 * 
	 * @param classObject
	 * @return
	 */
	public static <T> boolean isJsonValue(Class<T> classObject) {
		if (org.apache.commons.lang3.ClassUtils
				.isPrimitiveOrWrapper(classObject))
			return true;
		if (classObject.isAssignableFrom(String.class))
			return true;
		if (classObject.isAssignableFrom(Number.class))
			return true;
		return false;
	}

	public static boolean isAssignableFrom(Object child, Class<?> itemClass) {
		if (itemClass.isAnnotationPresent(JmentosEntity.class)) {
			String[] required = itemClass.getAnnotation(JmentosEntity.class)
					.required();
			if (required == null || required.length < 1)
				return true;
			else {
				for (String key : required) {
					if (!((JSONObject) child).has(key))
						return false;
				}
				return true;
			}
		}
		return false;
	}
}
