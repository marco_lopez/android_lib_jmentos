package com.tiscali.jmentos.convertors;

import android.util.Log;

import com.tiscali.jmentos.Commons;
import com.tiscali.jmentos.JmentosException;

public class StringConvertor implements ValueConvertor {

	private static final String tag = StringConvertor.class.getCanonicalName();

	public StringConvertor() {
		super();
	}

	@SuppressWarnings("unchecked")
	public <T> T convertFromValue(Object json, String format,
			Class<?> objectClass) throws JmentosException {
		if (objectClass == String.class) {
			try {
				String convert = (String) objectClass.cast(json);
				return (T) convert;
			} catch (Exception e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
				return null;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convertToValue(Object fieldObject, String format,
			Class<?> fieldClass) throws JmentosException {
		if (fieldObject == null)
			return null;

		if (fieldClass == String.class)
			return (T) fieldObject;

		return null;
	}
}
