package com.tiscali.jmentos.convertors;

import android.util.Log;

import com.tiscali.jmentos.Commons;
import com.tiscali.jmentos.JmentosException;

public class NumberConvertor implements ValueConvertor {

	private static final String tag = NumberConvertor.class.getCanonicalName();

	public NumberConvertor() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convertFromValue(Object json, String format,
			Class<?> objectClass) throws JmentosException {

		//

		Number number = null;

		try {
			number = (Number) json;
		} catch (ClassCastException e) {
			if (Commons.DEBUG)
				Log.e(tag, e.getMessage(), e);
			try {
				// json is representing a String.
				if (objectClass == Integer.class || objectClass == int.class)
					number = Integer.parseInt((String) json);

				if (objectClass == Double.class || objectClass == double.class)
					number = Double.parseDouble((String) json);

				if (objectClass == Long.class || objectClass == long.class)
					number = Long.parseLong((String) json);

				if (objectClass == Float.class || objectClass == float.class)
					number = Float.parseFloat((String) json);
			} catch (NumberFormatException nfe) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
				number = 0;
			}

		}

		Object object = number;

		if (objectClass == Integer.class || objectClass == int.class)
			object = number.intValue();

		if (objectClass == Double.class || objectClass == double.class)
			object = number.doubleValue();

		if (objectClass == Long.class || objectClass == long.class)
			object = number.longValue();

		if (objectClass == Float.class || objectClass == float.class)
			object = number.floatValue();

		//

		if (objectClass.isPrimitive())
			return (T) object;

		return (T) objectClass.cast(object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convertToValue(Object fieldObject, String format,
			Class<?> fieldClass) throws JmentosException {
		if (fieldObject == null)
			return null;

		Number number = (Number) fieldObject;

		Object json = number;

		if (fieldClass == Integer.class || fieldClass == int.class)
			json = number.intValue();

		if (fieldClass == Long.class || fieldClass == long.class)
			json = number.longValue();

		if (fieldClass == Double.class || fieldClass == double.class)
			json = number.doubleValue();

		if (fieldClass == Float.class || fieldClass == float.class)
			json = number.floatValue();

		if (fieldClass.isPrimitive())
			return (T) json;

		return (T) fieldClass.cast(json);
	}

}
