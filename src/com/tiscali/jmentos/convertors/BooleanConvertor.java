package com.tiscali.jmentos.convertors;

import android.util.Log;

import com.tiscali.jmentos.Commons;
import com.tiscali.jmentos.JmentosException;

public class BooleanConvertor implements ValueConvertor {

	private static final String tag = BooleanConvertor.class.getCanonicalName();

	@SuppressWarnings("unchecked")
	public <T> T convertFromValue(Object json, String format,
			Class<?> objectClass) throws JmentosException {

		Boolean bool = null;

		try {
			bool = (Boolean) json;
		} catch (ClassCastException e) {
			if (Commons.DEBUG)
				Log.e(tag, e.getMessage(), e);
			// json is representing a String.
			if (objectClass == Boolean.class || objectClass == boolean.class)
				bool = Boolean.parseBoolean((String) json);
		}

		Object object = bool.booleanValue();

		if (objectClass.isPrimitive())
			return (T) object;

		return (T) objectClass.cast(object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convertToValue(Object fieldObject, String format,
			Class<?> fieldClass) throws JmentosException {
		if (fieldObject == null)
			return null;

		if (fieldClass == boolean.class || fieldClass == Boolean.class)
			return (T) fieldObject;
		return null;
	}
}
