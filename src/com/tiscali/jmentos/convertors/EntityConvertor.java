package com.tiscali.jmentos.convertors;

import org.json.JSONException;
import org.json.JSONTokener;

import com.tiscali.jmentos.JmentosException;

public interface EntityConvertor {
	/**
	 * Entry point to start serialization
	 * 
	 * @param source
	 *            the java object to serialize
	 * @param model
	 *            the serialization class model
	 * @param target
	 *            the serialization target.
	 * @throws JmentosException
	 * @throws JSONException
	 */
	public void convertToJson(Object source, Class<?> model, Object target)
			throws JmentosException, JSONException;

	public <T> T newInstance(Class<T> objectClass) throws JmentosException;

	/**
	 * Entry point to start deserialization
	 * 
	 * @param source
	 *            the result of {@link JSONTokener#nextValue()} call
	 * @param target
	 *            the deserialization target
	 * @param model
	 *            the deserialization class model
	 * @throws JmentosException
	 */
	public void convertFromJSON(Object source, Object target, Class<?> model)
			throws JmentosException;
}
