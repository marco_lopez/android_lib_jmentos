package com.tiscali.jmentos.convertors;

import com.tiscali.jmentos.JmentosException;

public interface ValueConvertor {
	public <T> T convertToValue(Object fieldObject, String format,
			Class<?> fieldClass) throws JmentosException;

	public <T> T convertFromValue(Object jsonValue, String format,
			Class<?> javaClass) throws JmentosException;

}
