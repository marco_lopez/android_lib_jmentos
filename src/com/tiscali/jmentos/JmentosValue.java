package com.tiscali.jmentos;

import java.lang.annotation.*;

import com.tiscali.jmentos.convertors.*;

@Target(value={ElementType.FIELD, ElementType.METHOD})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface JmentosValue {
	public String name() default "";
	public String format() default "";
	public Class<? extends ValueConvertor> convertor() default ValueConvertor.class;
}
