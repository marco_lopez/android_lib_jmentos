package com.tiscali.jmentos;

@SuppressWarnings("serial")
public class JmentosException extends Exception {

	public JmentosException() {
		super();
	}

	public JmentosException(String arg0) {
		super(arg0);
	}

	public JmentosException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public JmentosException(Throwable arg0) {
		super(arg0);
	}

}
