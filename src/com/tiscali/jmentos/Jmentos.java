package com.tiscali.jmentos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.util.Log;

import com.tiscali.jmentos.convertors.BooleanConvertor;
import com.tiscali.jmentos.convertors.EntityConvertor;
import com.tiscali.jmentos.convertors.NumberConvertor;
import com.tiscali.jmentos.convertors.StringConvertor;
import com.tiscali.jmentos.convertors.ValueConvertor;
import com.tiscali.jmentos.utils.ClassUtils;
import com.tiscali.jmentos.utils.JSONUtils;

@SuppressWarnings("unused")
public class Jmentos implements EntityConvertor {
	private static Hashtable<Class<?>, Class<? extends EntityConvertor>> defaultEntityConvertors = new Hashtable<Class<?>, Class<? extends EntityConvertor>>();

	private static Hashtable<Class<?>, Class<? extends ValueConvertor>> defaultTextConvertors = new Hashtable<Class<?>, Class<? extends ValueConvertor>>();

	private static Hashtable<String, Jmentos> instances;

	private static final String tag = Jmentos.class.getCanonicalName();

	private static final String TAG = Jmentos.class.getSimpleName();

	private static Jmentos This;

	static {
		Jmentos.setDefaultValueConvertor(short.class, NumberConvertor.class);
		Jmentos.setDefaultValueConvertor(Short.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(int.class, NumberConvertor.class);
		Jmentos.setDefaultValueConvertor(Integer.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(long.class, NumberConvertor.class);
		Jmentos.setDefaultValueConvertor(Long.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(float.class, NumberConvertor.class);
		Jmentos.setDefaultValueConvertor(Float.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(double.class, NumberConvertor.class);
		Jmentos.setDefaultValueConvertor(Double.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(Number.class, NumberConvertor.class);

		Jmentos.setDefaultValueConvertor(String.class, StringConvertor.class);

		Jmentos.setDefaultValueConvertor(Boolean.class, BooleanConvertor.class);
		Jmentos.setDefaultValueConvertor(boolean.class, BooleanConvertor.class);
	}

	/**
	 * Returns the default Jmentos instance.
	 * 
	 * @return Jmentos default instance
	 * @throws JmentosException
	 */
	public static synchronized Jmentos getInstance() throws JmentosException {
		if (This == null) {
			This = new Jmentos("DEFAULT");
		}

		return This;
	}

	/**
	 * Returns the named Jmentos instance.
	 * 
	 * @param name
	 *            The instance name.
	 * @return Named Jmentos instance.
	 * @throws JmentosException
	 */
	public static synchronized Jmentos getInstance(String name)
			throws JmentosException {
		if (instances == null)
			instances = new Hashtable<String, Jmentos>();

		Jmentos instance = instances.get(name);
		if (instance == null) {
			instance = new Jmentos(name);
			instances.put(name, instance);
		}

		return instance;
	}

	public static JSONTokener parseDocument(File file)
			throws FileNotFoundException, IOException {
		InputStream inputstream = new FileInputStream(file);
		return parseDocument(inputstream);
	}

	public static JSONTokener parseDocument(InputStream inputStream)
			throws IOException {
		String source = IOUtils.toString(inputStream, "UTF-8");
		return parseDocument(source);
	}

	public static JSONTokener parseDocument(String source) {
		return new JSONTokener(source);
	}

	/**
	 * Set the default(shared with all Jmentos named instances) EntityConvertor
	 * class for the class specified.
	 * 
	 * @param entityClass
	 *            The class
	 * @param entityConvertorClass
	 *            The EntityConvertor class
	 */
	public static void setDefaultEntityConvertor(Class<?> entityClass,
			Class<? extends EntityConvertor> entityConvertorClass) {
		defaultEntityConvertors.put(entityClass, entityConvertorClass);
	}

	/**
	 * Set the default(shared with all Jmentos named instances) ValueConvertor
	 * class for the class specified.
	 * 
	 * @param textClass
	 *            The class
	 * @param textConvertorClass
	 *            The ValueConvertor class
	 */
	public static void setDefaultValueConvertor(Class<?> textClass,
			Class<? extends ValueConvertor> textConvertorClass) {
		defaultTextConvertors.put(textClass, textConvertorClass);
	}

	private int currentDepth = 0;

	SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private int depth = 1;

	private Hashtable<Class<?>, Class<? extends EntityConvertor>> entityConvertors = new Hashtable<Class<?>, Class<? extends EntityConvertor>>();

	private String mName;

	private Hashtable<Class<?>, Class<? extends ValueConvertor>> textConvertors = new Hashtable<Class<?>, Class<? extends ValueConvertor>>();

	private Jmentos(String name) throws JmentosException {
		this.mName = name;
	}

	/**
	 * Check if {@link JmentosEntity} annotation is present in model
	 * 
	 * @param model
	 *            the {@link Class} representing the model toconverto to.
	 * @throws JmentosException
	 *             if the model has no {@link JmentosEntity} annotation
	 */
	private void checkJmentosEntity(JmentosEntity annotation, Class<?> model)
			throws JmentosException {

		if (annotation == null)
			throw new JmentosException("The class " + model.getCanonicalName()
					+ " is not an JsonEntity");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tiscali.jmentos.convertors.EntityConvertor#convertFromJSON(java.lang
	 * .Object, java.lang.Object, java.lang.Class)
	 */
	@Override
	public void convertFromJSON(Object source, Object target, Class<?> model)
			throws JmentosException {
		JmentosEntity annotation = model.getAnnotation(JmentosEntity.class);
		checkJmentosEntity(annotation, model);
		if (source == null)
			return;
		if (Commons.VERBOSE)
			Log.i(tag,
					String.format("convertFromJSON: %s",
							model.getCanonicalName()));
		if (source instanceof JSONObject)
			convertFromJSONObject((JSONObject) source, target, model);
		else if (source instanceof JSONArray)
			convertFromJSONArray((JSONArray) source, target, model);
	}

	/**
	 * Converts a {@link JSONArray} to a corresponding Java object.
	 * 
	 * @param source
	 *            the {@link JSONArray} source
	 * @param target
	 *            the Java {@link Object} target
	 * @param model
	 *            the {@link Class} model
	 * @throws JmentosException
	 */
	public void convertFromJSONArray(JSONArray source, Object target,
			Class<?> model) throws JmentosException {
		// super class check
		Class<?> superClass = model.getSuperclass();
		if (superClass.isAnnotationPresent(JmentosEntity.class)
				&& superClass
						.getAnnotation(JmentosEntity.class)
						.type()
						.equals(com.tiscali.jmentos.JmentosEntity.Type.JmentosArray))
			convertFromJSONArray(source, target, superClass);
		// parse the field of
		for (Field field : model.getDeclaredFields()) {
			boolean isAccessible = field.isAccessible();
			if (!isAccessible)
				field.setAccessible(true);
			try {
				Object value = null;

				if (field.isAnnotationPresent(JmentosArray.class)) {
					value = getFromJsonArray(
							field.getAnnotation(JmentosArray.class),
							field.getName(), field.getType(),
							field.getGenericType(), source);
				}
				if (value != null) {
					field.set(target, value);
				}
			} catch (IllegalAccessException e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
				throw new JmentosException(e);
			} finally {
				if (!isAccessible)
					field.setAccessible(false);
			}
		}
	}

	/**
	 * Deserialize a {@link JSONObject} into a Java {@link Object}.
	 * 
	 * @param source
	 *            A {@link JSONObject}, source of the deserialization
	 * @param target
	 *            A {@link Object}, target of the deserialization
	 * @param model
	 *            A {@link Class} defining the model underlaying the
	 *            deserialization
	 * @throws JmentosException
	 */
	public void convertFromJSONObject(JSONObject source, Object target,
			Class<?> model) throws JmentosException {
		JmentosEntity annotation = model.getAnnotation(JmentosEntity.class);
		// check if annotation is present
		checkJmentosEntity(annotation, model);
		if (source == null)
			return;
		// super class check
		Class<?> superClass = model.getSuperclass();
		if (superClass.isAnnotationPresent(JmentosEntity.class))
			convertFromJSONObject(source, target, superClass);
		// collection check: the object may represent a collection also if
		// source is a JSONObject: hashmap
		if (annotation.type().equals(
				com.tiscali.jmentos.JmentosEntity.Type.JmentosArray)) {
			convertJSONObject(source, target, model);
		} else {
			for (Field field : model.getDeclaredFields()) {
				boolean isAccessible = field.isAccessible();
				if (!isAccessible)
					field.setAccessible(true);

				try {
					Object fieldObject = null;

					if (field.isAnnotationPresent(JmentosValue.class)) {
						fieldObject = getJmentosValueFromParent(
								field.getAnnotation(JmentosValue.class),
								field.getName(), field.getType(), source);
					} else if (field.isAnnotationPresent(JmentosObject.class)) {
						fieldObject = getJmentosObjectFromParent(
								field.getAnnotation(JmentosObject.class),
								field.getName(), field.getType(), source);
					} else if (field.isAnnotationPresent(JmentosArray.class)) {
						fieldObject = getJmentosArrayFromJSONObject(
								field.getAnnotation(JmentosArray.class),
								field.getName(), field.getType(),
								field.getGenericType(), source);
					}
					if (fieldObject != null) {
						field.set(target, fieldObject);
					}
				} catch (IllegalAccessException e) {
					if (Commons.DEBUG)
						Log.e(tag, e.getMessage(), e);
					throw new JmentosException(e);
				} finally {
					if (!isAccessible)
						field.setAccessible(false);
				}
			}
		}
	}

	/**
	 * Deserialize a {@link JSONObject} into a Java {@link Collection} amd
	 * stores it into a field of the target.
	 * 
	 * @param source
	 *            A {@link JSONObject}, source of the deserialization
	 * @param target
	 *            A {@link Object}, target of the deserialization
	 * @param model
	 *            A {@link Class} defining the model underlaying the
	 *            deserialization
	 * @throws JmentosException
	 */
	public void convertJSONObject(JSONObject source, Object target,
			Class<?> model) throws JmentosException {
		// super class check
		Class<?> superClass = model.getSuperclass();
		if (superClass.isAnnotationPresent(JmentosEntity.class)
				&& superClass
						.getAnnotation(JmentosEntity.class)
						.type()
						.equals(com.tiscali.jmentos.JmentosEntity.Type.JmentosArray))
			convertJSONObject(source, target, superClass);
		// parse the field of
		//
		for (Field field : model.getDeclaredFields()) {
			boolean isAccessible = field.isAccessible();
			if (!isAccessible)
				field.setAccessible(true);
			try {
				Object fieldObject = null;
				if (field.isAnnotationPresent(JmentosArray.class)) {
					fieldObject = getCollectionFromJSONObject(
							field.getAnnotation(JmentosArray.class),
							field.getName(), field.getType(),
							field.getGenericType(), source);
				}
				if (fieldObject != null) {
					field.set(target, fieldObject);
				}
			} catch (IllegalAccessException e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
				throw new JmentosException(e);
			} finally {
				if (!isAccessible)
					field.setAccessible(false);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tiscali.jmentos.convertors.EntityConvertor#convertToJson(java.lang
	 * .Object, java.lang.Class, java.lang.Object)
	 */
	public void convertToJson(Object source, Class<?> model, Object target)
			throws JmentosException, JSONException {
		JmentosEntity annotation = model.getAnnotation(JmentosEntity.class);
		if (annotation == null)
			throw new JmentosException("The class " + model.getSimpleName()
					+ " is not an JsonEntity");

		if (source == null)
			return;

		Class<?> superClass = model.getSuperclass();

		if (superClass.isAnnotationPresent(JmentosEntity.class))
			convertToJson(source, superClass, target);

		try {

			for (Field field : model.getDeclaredFields()) {
				boolean isAccessible = field.isAccessible();
				if (!isAccessible)
					field.setAccessible(true);

				try {
					Object fieldObject = field.get(source);
					if (fieldObject == null)
						continue;

					if (field.isAnnotationPresent(JmentosValue.class)) {
						toJsonValue(field.getAnnotation(JmentosValue.class),
								field.getName(), field.getType(), fieldObject,
								target);
					} else if (field.isAnnotationPresent(JmentosObject.class)) {
						toJsonObject(field.getAnnotation(JmentosObject.class),
								field.getName(), field.getType(), fieldObject,
								target);
					} else if (field.isAnnotationPresent(JmentosArray.class)) {
						toJsonArray(field.getAnnotation(JmentosArray.class),
								field.getName(), field.getType(), fieldObject,
								target);
					}
				} finally {
					if (!isAccessible)
						field.setAccessible(false);
				}
			}

		} catch (IllegalAccessException e) {
			if (Commons.DEBUG)
				Log.e(tag, e.getMessage(), e);
			throw new JmentosException(e);
		}
	}

	/**
	 * Return the objectClass associated EntityConvertor instance.
	 * 
	 * @param model
	 * @return The EntityConvertor instance.
	 * @throws JmentosException
	 */
	public EntityConvertor getEntityConvertor(Class<?> model)
			throws JmentosException {
		EntityConvertor entityConvertor = null;

		if (model.isAnnotationPresent(JmentosEntity.class)) {
			entityConvertor = this;
		} else if (entityConvertors.get(model) != null) {
			entityConvertor = newInstance(entityConvertors.get(model));
		} else if (defaultEntityConvertors.get(model) != null) {
			entityConvertor = newInstance(defaultEntityConvertors.get(model));
		} else {
			throw new JmentosException(
					"No EntityConvertor available for class "
							+ model.getSimpleName());
		}
		return entityConvertor;
	}

	/**
	 * Method to obtain an ArrayList from JSONArray.
	 * 
	 * @param annotation
	 * @param fieldName
	 * @param fieldClass
	 * @param fieldType
	 * @param parent
	 * @return
	 * @throws JmentosException
	 */
	@SuppressWarnings({ "unchecked" })
	public Object getFromJsonArray(JmentosArray annotation, String fieldName,
			Class<?> fieldClass, Type fieldType, JSONArray parent)
			throws JmentosException {
		if (parent == null)
			return null;
		// get the class of the item
		// Object
		Class<?> itemClass = annotation.itemClass();
		// if no specific itemClass is defined it's read from ParameterizedType
		if (itemClass == Object.class) {
			if (fieldType instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) fieldType;
				Type[] actualTypeArguments = parameterizedType
						.getActualTypeArguments();
				if (actualTypeArguments.length > 0
						&& actualTypeArguments[0] instanceof Class<?>)
					itemClass = (Class<?>) actualTypeArguments[0];
			} else if (fieldClass.isArray()) {
				itemClass = fieldClass.getComponentType();
			}
		}
		boolean isJsonValue = false;
		// we must detect which convertor to use
		ValueConvertor valueConvertor = null;
		EntityConvertor entityConvertor = null;
		// itemClass is derived from
		if (ClassUtils.isJsonValue(itemClass)) {
			valueConvertor = getValueConvertor(itemClass);
		} else {
			// is JsonObject
			// at the moment array of array are not considered
			if (annotation.entityConvertor() != Jmentos.class) {
				entityConvertor = newInstance(annotation.entityConvertor());
			} else {
				entityConvertor = getEntityConvertor(itemClass);
			}
		}
		// creates the new arrayList instance
		List<Object> arrayList = newInstance(annotation.listClass());
		//
		for (int i = 0; i < parent.length(); i++) {
			Object child = null;
			try {
				child = parent.get(i);
			} catch (Exception e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
			}
			Class<?> objectClass = child.getClass();
			// JSONArray.get(i) return always
			// an org.json model object
			// if (!(child.getClass().isAssignableFrom(itemClass)))
			// continue;
			if (ClassUtils.isJsonValue(itemClass)) {
				arrayList.add(valueConvertor.convertFromValue(child, "",
						itemClass));
			} else if (!ClassUtils.isAssignableFrom(child, itemClass))
				continue;
			else {
				Object itemObject = entityConvertor.newInstance(itemClass);

				entityConvertor.convertFromJSON(child, itemObject, itemClass);
				arrayList.add(itemObject);
			}
		}
		if (fieldClass.isArray()) {
			Object o = Array.newInstance(fieldClass.getComponentType(),
					arrayList.size());
			for (int i = 0; i < arrayList.size(); i++) {
				Array.set(o, i, arrayList.get(i));
			}
			return o;
		}
		return arrayList;
	}

	/**
	 * Convert a {@link JSONArray} into a {@link Collection}
	 * 
	 * @param annotation
	 *            the annotation of the field
	 * @param name
	 *            the name of the field
	 * @param model
	 *            the {@link Class} of the field
	 * @param genericType
	 *            the {@link Type} of the field
	 * @param source
	 *            the {@link JSONArray} to parse @return An Object representing
	 *            a Collection
	 * @throws JmentosException
	 */
	private Object getCollectionFromJSONArray(JmentosArray annotation,
			String name, Class<?> model, Type genericType, JSONArray source)
			throws JmentosException {
		// the Collection item class
		Class<?> itemModel = annotation.itemClass();
		// if no specific itemClass is defined it's read from ParameterizedType
		if (itemModel == Object.class) {
			if (genericType instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) genericType;
				Type[] actualTypeArguments = parameterizedType
						.getActualTypeArguments();
				if (actualTypeArguments.length > 0
						&& actualTypeArguments[0] instanceof Class<?>)
					itemModel = (Class<?>) actualTypeArguments[0];
			} else if (model.isArray()) {
				itemModel = model.getComponentType();
			}
		}
		// obtains a list of value
		if (ClassUtils.isJsonValue(itemModel)) {
			List<Object> valueList = newInstance(annotation.listClass());
			for (int i = 0; i < source.length(); i++) {
				valueList
						.add(getJmentosValueFromJSONArray(i, itemModel, source));
			}
			valueList.removeAll(Collections.singleton(null));
			if (model.isArray()) {
				Object o = Array.newInstance(model.getComponentType(),
						valueList.size());
				for (int i = 0; i < valueList.size(); i++) {
					Array.set(o, i, valueList.get(i));
				}
				return o;
			}
			return valueList;
		} else {
			// itemModel represents an object
			EntityConvertor entityConvertor = null;

			if (annotation.entityConvertor() != Jmentos.class) {
				entityConvertor = newInstance(annotation.entityConvertor());
			} else {
				entityConvertor = getEntityConvertor(itemModel);
			}

			// creates the new arrayList instance
			List<Object> arrayList = newInstance(annotation.listClass());
			int length = source.length();
			for (int i = 0; i < length; i++) {
				try {
					Object child = null;
					child = ((JSONArray) source).get(i);

					// item class

					Class<? extends Object> childClass;
					if (null != child)
						childClass = child.getClass();
					else
						continue;

					if (!ClassUtils.isAssignableFrom(child, itemModel))
						continue;
					else {
						Object itemObject = entityConvertor
								.newInstance(itemModel);
						entityConvertor.convertFromJSON(child, itemObject,
								itemModel);
						arrayList.add(itemObject);
					}
				} catch (JmentosException e) {
					if (Commons.DEBUG)
						Log.e(tag, e.getMessage(), e);
				} catch (JSONException e) {
					if (Commons.DEBUG)
						Log.e(tag, e.getMessage(), e);
				}

			}
			if (model.isArray()) {
				Object o = Array.newInstance(model.getComponentType(),
						arrayList.size());
				for (int i = 0; i < arrayList.size(); i++) {
					Array.set(o, i, arrayList.get(i));
				}
				return o;
			}
			return arrayList;
		}
	}

	/**
	 * Convert a {@link JSONObject} into a {@link Collection}
	 * 
	 * @param annotation
	 *            the annotation of the field
	 * @param name
	 *            the name of the field
	 * @param model
	 *            the {@link Class} of the field
	 * @param genericType
	 *            the {@link Type} of the field
	 * @param source
	 *            the {@link JSONObject} to parse
	 * @return An Object representing a Collection
	 * @throws JmentosException
	 */
	private Object getCollectionFromJSONObject(JmentosArray annotation,
			String name, Class<?> model, Type genericType, JSONObject source)
			throws JmentosException {
		String sortPattern = annotation.sortPattern();
		boolean sortByKey = annotation.sortByKey();
		boolean isMap = annotation.isMap();
		SortedSet<String> keys = null;
		Comparator<String> comparator;
		try {
			comparator = (Comparator<String>) (annotation.comparator() != null ? annotation
					.comparator().newInstance() : null);
		} catch (Exception e) {
			comparator = null;
		}
		if (isMap == false)
			return null;
		// order does matter
		if (sortByKey == true) {
			String pattern = annotation.sortPattern();
			Iterator<String> iterator = source.keys();
			while (iterator.hasNext()) {
				String string = iterator.next();
				// string match pattern
				if (Pattern.matches(pattern, string)) {
					if (null == keys)
						try {
							keys = new TreeSet<String>(comparator);
						} catch (Exception e) {
							keys = new TreeSet<String>();
						}
					keys.add(string);
				}
			}
			if (null != keys && keys.size() > 0) {
				String[] keysArray = JSONUtils.toStringArray(keys);
				return getJmentosArrayFromJSONObjectAsMap(annotation, name,
						model, genericType, source, keysArray);

			} else
				return null;
		}
		// order doesn't matter
		else {
			if (Commons.VERBOSE)
				Log.e(tag, String.format(
						">>>>>>> pattern and order don't matter: %s",
						model.getCanonicalName()));
			JSONArray names = source.names();
			if (null != names && names.length() > 0) {
				String[] keysArray = JSONUtils.toStringArray(names);
				return getJmentosArrayFromJSONObjectAsMap(annotation, name,
						model, genericType, source, keysArray);

			} else
				return null;
		}
	}

	/**
	 * Get a {@link Collection} from a value of a {@link JSONObject}
	 * 
	 * @param annotation
	 *            the annotation of the field
	 * @param name
	 *            the name of the field
	 * @param model
	 *            the {@link Class} of the field
	 * @param genericType
	 *            the {@link Type} of the field
	 * @param parent
	 *            {@link JSONObject} the value is sourced by
	 * @return An Object representing a Collection
	 * @throws JmentosException
	 */
	public Object getJmentosArrayFromJSONObject(JmentosArray annotation,
			String name, Class<?> model, Type genericType, JSONObject parent)
			throws JmentosException {
		if (Commons.VERBOSE)
			Log.w(tag,
					String.format("getJmentosArrayFromJSONObject: %s",
							model.getCanonicalName()));
		String key = annotation.name();
		if (key.length() == 0)
			key = name;

		if (null != parent.optJSONArray(key))
			return getCollectionFromJSONArray(annotation, name, model,
					genericType, parent.optJSONArray(key));
		else if (null != parent.optJSONObject(key))
			return getCollectionFromJSONObject(annotation, name, model,
					genericType, parent.optJSONObject(key));
		else
			return null;
	}

	/**
	 * Get Java {@link Object} member from a parent Java {@link Object}, sourced
	 * by parent {@link JSONObject}.
	 * 
	 * @param annotation
	 *            {@link JmentosObject} annotation related to model
	 * @param name
	 *            the field name.
	 * @param model
	 *            the Class associated with the type of this field.
	 * @param parent
	 *            the parent {@link Object}
	 * @return a Java Object based on model
	 * @throws JmentosException
	 * 
	 * 
	 * @author marcolopez
	 * @category List, Object
	 */
	public Object getJmentosObjectFromParent(JmentosObject annotation,
			String name, Class<?> model, JSONObject parent)
			throws JmentosException {
		String key = annotation.name();
		if (key.length() == 0)
			key = name;
		if (Commons.VERBOSE)
			Log.w(tag, String.format("getJmentosObjectFromParent: %s %s", key,
					model.getCanonicalName()));

		EntityConvertor entityConvertor = null;
		if (annotation.convertor() != Jmentos.class) {
			entityConvertor = newInstance(annotation.convertor());
		} else {
			entityConvertor = getEntityConvertor(model.isArray() ? model
					.getComponentType() : model);
		}

		try {
			JSONObject jsonObject = parent.optJSONObject(key);

			if (jsonObject != null) {
				Object javaObject = entityConvertor.newInstance(model);

				entityConvertor.convertFromJSON(jsonObject, javaObject, model);

				return javaObject;
			}
		} catch (Exception e) {
			if (Commons.DEBUG)
				Log.e(tag, e.getMessage(), e);
		}
		return null;

	}

	public Object getJmentosValueFromJSONArray(int i, Class<?> model,
			JSONArray parent) {
		if (model.equals(boolean.class))
			return parent.optBoolean(i);
		else if (model.equals(double.class))
			return parent.optDouble(i);
		else if (model.equals(int.class))
			return parent.optInt(i);
		else if (model.equals(long.class))
			return parent.optLong(i);
		else if (model.equals(String.class)) {
			if (parent.isNull(i))
				return null;
			return parent.optString(i);
		}
		Log.e(TAG, "Unknown JmentosValue model: " + model.getCanonicalName());
		return null;
	}

	/**
	 * getJsonSimpleValue( field.getAnnotation(JsonValue.class),
	 * field.getName(), field.getType(), json);
	 * 
	 * @param annotation
	 * @param name
	 * @param model
	 * @param parent
	 * @return
	 * @throws JmentosException
	 */
	public Object getJmentosValueFromParent(JmentosValue annotation,
			String name, Class<?> model, JSONObject parent)
			throws JmentosException {
		String key = annotation.name();
		if (key.length() == 0)
			key = name;
		if (Commons.VERBOSE)
			Log.w(tag, String.format("getJmentosValueFromParent: %s, %s", key,
					model.getCanonicalName()));
		if (model.equals(boolean.class))
			return parent.optBoolean(key);
		else if (model.equals(double.class))
			return parent.optDouble(key);
		else if (model.equals(int.class))
			return parent.optInt(key);
		else if (model.equals(long.class))
			return parent.optLong(key);
		else if (model.equals(String.class)) {
			if (parent.isNull(key))
				return null;
			return parent.optString(key);
		}
		Log.e(TAG, "Unknown JmentosValue model: " + model.getCanonicalName());
		return null;
	}

	/**
	 * 
	 * @return The name of Jmentos instance.
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Get a {@link Collection} from a value of a {@link JSONObject}
	 * 
	 * @param annotation
	 *            the annotation of the field
	 * @param name
	 *            the name of the field
	 * @param model
	 *            the {@link Class} of the field
	 * @param genericType
	 *            the {@link Type} of the field
	 * @param parent
	 *            {@link JSONObject} the value is sourced by
	 * @param keys
	 *            an array representing the entrySet of the object to be
	 *            converted to collection
	 * @return An Object representing a Collection
	 * @throws JmentosException
	 */
	public Object getJmentosArrayFromJSONObjectAsMap(JmentosArray annotation,
			String name, Class<?> model, Type genericType, JSONObject parent,
			String[] keys) throws JmentosException {
		if (Commons.VERBOSE)
			Log.w(tag, String.format("getJmentosArrayFromJSONObjectAsMap: %s",
					model.getCanonicalName()));
		Class<?> itemModel = annotation.itemClass();
		// if no specific itemClass is defined it's read from ParameterizedType
		if (itemModel == Object.class) {
			if (genericType instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) genericType;
				Type[] actualTypeArguments = parameterizedType
						.getActualTypeArguments();
				if (actualTypeArguments.length > 0
						&& actualTypeArguments[0] instanceof Class<?>)
					itemModel = (Class<?>) actualTypeArguments[0];
			} else if (model.isArray()) {
				itemModel = model.getComponentType();
			}
		}

		// once item class is detected we must detect which convertor to use
		boolean isJsonValue = false;
		// ------------
		ValueConvertor valueConvertor = null;
		EntityConvertor entityConvertor = null;
		//
		if (ClassUtils.isJsonValue(itemModel)) {
			// itemClass represent a primitive
			valueConvertor = getValueConvertor(itemModel);
		} else {
			// itemClass represent an object
			if (annotation.entityConvertor() != Jmentos.class) {
				entityConvertor = newInstance(annotation.entityConvertor());
			} else {
				entityConvertor = getEntityConvertor(itemModel);
			}
		}
		// creates the new arrayList instance
		List<Object> arrayList = newInstance(annotation.listClass());
		int length = keys.length;
		// parse the members
		for (int i = 0; i < length; i++) {
			try {// get item
				String key = keys[i];
				Object child = parent.get(key);
				Class<? extends Object> childClass;
				if (null != child)
					childClass = child.getClass();
				else
					continue;

				if (ClassUtils.isJsonValue(itemModel)) {

					arrayList.add(valueConvertor.convertFromValue(child, "",
							itemModel));

				} else if (!ClassUtils.isAssignableFrom(child, itemModel))
					continue;
				else {
					Object itemObject = entityConvertor.newInstance(itemModel);

					entityConvertor.convertFromJSON(child, itemObject,
							itemModel);
					arrayList.add(itemObject);
				}
			} catch (JmentosException e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
			} catch (JSONException e) {
				if (Commons.DEBUG)
					Log.e(tag, e.getMessage(), e);
			}
		}
		if (model.isArray()) {
			Object o = Array.newInstance(model.getComponentType(),
					arrayList.size());
			for (int i = 0; i < arrayList.size(); i++) {
				Array.set(o, i, arrayList.get(i));
			}
			return o;
		}
		return arrayList;
	}

	/**
	 * Return the objectClass associated ValueConvertor instance.
	 * 
	 * @param model
	 * @return The ValueConvertor instance.
	 * @throws JmentosException
	 */
	public ValueConvertor getValueConvertor(Class<?> model)
			throws JmentosException {
		ValueConvertor convertor = null;

		if (textConvertors.get(model) != null) {
			convertor = newInstance(textConvertors.get(model));
		} else if (defaultTextConvertors.get(model) != null) {
			convertor = newInstance(defaultTextConvertors.get(model));
		} else {
			throw new JmentosException("No ValueConvertor available for class "
					+ model.getSimpleName());
		}

		return convertor;
	}

	public <T> T newInstance(Class<T> objectClass) throws JmentosException {
		try {
			Constructor<T> constructor = objectClass
					.getDeclaredConstructor(new Class[] {});
			boolean isAccessible = constructor.isAccessible();
			if (!isAccessible)
				constructor.setAccessible(true);

			try {
				return constructor.newInstance(new Object[] {});
			} finally {
				if (!isAccessible)
					constructor.setAccessible(false);
			}
		} catch (Exception e) {
			if (Commons.DEBUG)
				Log.e(tag, e.getMessage(), e);
			throw new JmentosException("The class " + objectClass.getName()
					+ " must have a default constructor");
		}
	}

	/**
	 * @param <T>
	 *            Converted Object instance type
	 * @param objectClass
	 *            Object class to bind
	 * @param file
	 *            The json Source
	 * @return objectClass binded Object
	 * @throws FileNotFoundException
	 * @throws JmentosException
	 * @throws JSONException
	 * @throws IOException
	 */
	public <T> T readFrom(Class<T> objectClass, File file)
			throws FileNotFoundException, JmentosException, JSONException,
			IOException {
		return readFrom(objectClass, parseDocument(file));
	}

	/**
	 * Loads the object with the provided source.
	 * 
	 * @param <T>
	 *            Converted Object instance type
	 * @param objectClass
	 *            Object class to bind
	 * @param inputStream
	 *            The json Source
	 * @return objectClass binded Object
	 * @throws JmentosException
	 * @throws JSONException
	 * @throws IOException
	 */
	public <T> T readFrom(Class<T> objectClass, InputStream inputStream)
			throws JmentosException, JSONException, IOException {
		return readFrom(objectClass, parseDocument(inputStream));
	}

	/**
	 * @param <T>
	 *            Converted Object instance type
	 * @param objectClass
	 *            Object class to bind
	 * @param tokener
	 *            The json Source
	 * @return objectClass binded Object
	 * @throws JmentosException
	 * @throws JSONException
	 */
	public <T> T readFrom(Class<T> objectClass, JSONTokener tokener)
			throws JmentosException, JSONException {
		return readFrom(objectClass, tokener.nextValue());
	}

	public <T> T readFrom(Class<T> objectClass, Object json)
			throws JmentosException {
		EntityConvertor entityConvertor = getEntityConvertor(objectClass);

		T object = entityConvertor.newInstance(objectClass);
		entityConvertor.convertFromJSON(json, object, objectClass);

		return object;
	}

	/**
	 * Loads the object with the provided source.
	 * 
	 * @param <T>
	 *            Converted Object instance type
	 * @param objectClass
	 *            Object class to bind
	 * @param source
	 *            The json Source
	 * @return objectClass binded Object
	 * @throws JSONException
	 * @throws JmentosException
	 */
	public <T> T readFrom(Class<T> objectClass, String source)
			throws JmentosException, JSONException {
		return readFrom(objectClass, parseDocument(source));
	}

	/**
	 * Set the EntityConvertor class for the class specified.
	 * 
	 * @param entityClass
	 *            The class
	 * @param entityConvertorClass
	 *            The EntityConvertor class
	 */
	public void setEntityConvertor(Class<?> entityClass,
			Class<? extends EntityConvertor> entityConvertorClass) {
		entityConvertors.put(entityClass, entityConvertorClass);
	}

	/**
	 * Set the ValueConvertor class for the class specified.
	 * 
	 * @param textClass
	 *            The class
	 * @param textConvertorClass
	 *            The ValueConvertor class
	 */
	public void setTextConvertor(Class<?> textClass,
			Class<? extends ValueConvertor> textConvertorClass) {
		textConvertors.put(textClass, textConvertorClass);
	}

	private Object toJson(Object item) throws JmentosException, JSONException {
		if (item == null)
			return null;

		Class<? extends Object> itemClass = item.getClass();

		ValueConvertor valueConvertor = null;
		EntityConvertor entityConvertor = null;
		if (ClassUtils.isJsonValue(itemClass)) {
			valueConvertor = getValueConvertor(itemClass);
			return valueConvertor.convertToValue(item, null, itemClass);
		}
		if (itemClass.isAnnotationPresent(JmentosObject.class)) {
			entityConvertor = getEntityConvertor(itemClass);
			JSONObject jsonObject = new JSONObject();
			entityConvertor.convertToJson(item, itemClass, jsonObject);
			return jsonObject;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	private void toJsonArray(JmentosArray annotation, String fieldName,
			Class<?> fieldClass, Object fieldObject, Object json)
			throws JmentosException, JSONException {
		String key = annotation.name();
		if (key.length() == 0)
			key = fieldName;

		if (!List.class.isAssignableFrom(fieldClass))
			throw new JmentosException("The class "
					+ fieldClass.getSimpleName() + " is not a List");

		JSONArray jsonArray;

		if (((JSONObject) json).has(key))
			jsonArray = ((JSONObject) json).getJSONArray(key);

		else
			jsonArray = new JSONArray();

		for (Object item : (List) fieldObject) {
			jsonArray.put(toJson(item));
			// listElement.appendChild(toElement(item, document));
		}

		((JSONObject) json).put(key, jsonArray);
	}

	private void toJsonObject(JmentosObject annotation, String fieldName,
			Class<?> fieldClass, Object fieldObject, Object jsonObject)
			throws JmentosException, JSONException {
		String key = annotation.name();
		if (key.length() == 0)
			key = fieldName;

		EntityConvertor entityConvertor = this;
		if (annotation.convertor() != Jmentos.class) {
			entityConvertor = newInstance(annotation.convertor());
		} else {
			entityConvertor = getEntityConvertor(fieldClass);
		}

		JSONObject fieldElement = new JSONObject();
		entityConvertor.convertToJson(fieldObject, fieldClass, fieldElement);
		((JSONObject) jsonObject).put(key, fieldElement);
	}

	/**
	 * Convert the object to Element.
	 * 
	 * @param javaObject
	 * @param document
	 * @return
	 * @throws JmentosException
	 * @throws JSONException
	 */
	public JSONObject toJSONObject(Object javaObject) throws JmentosException,
			JSONException {
		if (javaObject == null)
			return null;

		Class<? extends Object> javaClass = javaObject.getClass();

		JmentosEntity annotation = javaClass.getAnnotation(JmentosEntity.class);

		EntityConvertor entityConvertor = getEntityConvertor(javaClass);

		JSONObject jsonObject = new JSONObject();
		entityConvertor.convertToJson(javaObject, javaClass, jsonObject);
		return jsonObject;
	}

	private void toJsonValue(JmentosValue annotation, String fieldName,
			Class<?> fieldClass, Object fieldObject, Object jsonObject)
			throws JmentosException, JSONException {
		String key = annotation.name();
		if (key.length() == 0)
			key = fieldName;

		ValueConvertor valueConvertor = null;

		if (annotation.convertor() != ValueConvertor.class) {
			valueConvertor = newInstance(annotation.convertor());
		} else {
			valueConvertor = getValueConvertor(fieldClass);
		}

		((JSONObject) jsonObject).put(key, valueConvertor.convertToValue(
				fieldObject, annotation.format(), fieldClass));
	}
}
