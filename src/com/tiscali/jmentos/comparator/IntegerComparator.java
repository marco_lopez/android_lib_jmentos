package com.tiscali.jmentos.comparator;

import java.util.Comparator;

public class IntegerComparator implements Comparator<String> {

	@Override
	public int compare(String lhs, String rhs) {
		return Integer.parseInt(lhs) - Integer.parseInt(rhs);
	}
}
