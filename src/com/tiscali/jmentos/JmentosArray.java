package com.tiscali.jmentos;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.json.JSONObject;

import android.app.PendingIntent.CanceledException;

import com.tiscali.jmentos.comparator.StringComparator;
import com.tiscali.jmentos.convertors.EntityConvertor;
import com.tiscali.jmentos.convertors.StringConvertor;
import com.tiscali.jmentos.convertors.ValueConvertor;

@Target(value = { ElementType.FIELD, ElementType.METHOD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JmentosArray {
	public String name() default "";

	@SuppressWarnings("rawtypes")
	public Class<? extends Comparator> comparator() default StringComparator.class;

	/**
	 * Flag used when converting {@link JSONObject} to Java {@link Collection},
	 * to determine if keys sorting must be respected
	 * 
	 * @return <code>true</code> if JSONObject {@link CanceledException} be
	 *         converted, <code>false</code>
	 */
	public boolean sortByKey() default false;

	/**
	 * Flag used when parsing {@link JSONObject} bound to {@link JmentosArray}
	 * to determine if it can be converted to Java {@link Collection}
	 * 
	 * @return <code>true</code> if JSONObject {@link CanceledException} be
	 *         converted, <code>false</code>
	 */
	public boolean isMap() default false;

	/**
	 * String pattern used as matching criterium for {@link JSONObject} key to
	 * determine if corresponding value can be added to the {@link Collection}
	 * 
	 * @return a String to match the key against to.
	 */
	public String sortPattern() default ".*";

	@SuppressWarnings("rawtypes")
	public Class<? extends List> listClass() default ArrayList.class;

	/**
	 * The class used as model for the collection's items,
	 * 
	 * @return a {@link Class} used as model for the collection's items.
	 */
	public Class<?> itemClass() default Object.class;

	/**
	 * The class used to convert the {@link JSONObject} members, if they are
	 * bound to {@link JmentosEntity}.
	 * 
	 * @return the {@link EntityConvertor} used to convert
	 */
	public Class<? extends EntityConvertor> entityConvertor() default Jmentos.class;

	public Class<? extends ValueConvertor> valueConvertor() default StringConvertor.class;
}
